// >>>>>>>>>>>>> CALCULATOR

var output = document.getElementById("output");
var outTab = document.getElementById("tab");

var num1 = document.getElementById("number1");
var num2 = document.getElementById("number2");



function addBy() {

    var a = num1.value + " + " + num2.value;
    var b = Number(num1.value) + Number(num2.value);

    if(isNaN(num1.value) || isNaN(num2.value)){
        outTab.innerText = " Please enter a Valid number"
    }else{
        outHtml(a,b);
    }
}

function subBy() {

    var a = num1.value + " - " + num2.value;
    var b = Number(num1.value) - Number(num2.value);

    if(isNaN(num1.value) || isNaN(num2.value)){
        outTab.innerText = " Please enter a Valid number"
    }else{
        outHtml(a,b);
    }
}

function mulBy() {

    var a = num1.value + " x " + num2.value;
    var b = Number(num1.value) * Number(num2.value);

    if(isNaN(num1.value) || isNaN(num2.value)){
        outTab.innerText = " Please enter a Valid number"
    }else{
        outHtml(a,b);
    }
}

function divBy() {

    var a = num1.value + " / " + num2.value;
    var b = Number(num1.value) / Number(num2.value);

    if(isNaN(num1.value) || isNaN(num2.value) ){
        outTab.innerText = " Please enter a Valid number"
    }else{
        outHtml(a,b);
    }
}

function outHtml(x , y) {

    output.innerHTML = x + " = " + y;

}